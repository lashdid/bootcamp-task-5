import { Grid, Card, CardHeader, CardMedia, CardContent, Typography, Avatar } from '@mui/material'
import styles from './styles.module.css'
import { useEffect, useState } from 'react'
import axios from 'axios'
import "@fontsource/dongle/400.css"

export const GridContent = () => {
  const apiEndpoint = 'http://localhost:3004/postgenerated'
  const [data, setData] = useState([])

  useEffect(() => {
    axios.get(apiEndpoint).then((res) => setData(res.data)).catch(() => console.error('Endpoint api salah, periksa di component GridContent'))
  }, [])

  return (
    <Grid container className={styles.container} columnSpacing={1}>
      {data.length ? data.map((item, idx) => (
        <Grid key={item.post_id} item xs={12} sm={6}>
          <Card style={{height: '100%'}}>
            <CardHeader
            style={{fontFamily: 'Dongle'}}
              avatar={<Avatar style={{backgroundColor: 'red'}} aria-label="recipe">{item.author[0]}</Avatar>}
              title={item.title}
              subheader={item.datePost}
            />
            <CardMedia
              component="img"
              height="194"
              image={item.img}
              alt={item.post_id}
            />
            <CardContent>
              <Typography variant="body2" color="text.secondary" style={{fontFamily: 'Dongle', fontSize: '25px'}}>
                {item.description}
              </Typography>
            </CardContent>
          </Card>
        </Grid>
      )) : <p>Data tidak ditemukan, tolong periksa console</p>}
    </Grid>
  )
}
