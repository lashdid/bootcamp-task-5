import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import IconButton from "@mui/material/IconButton";
import CameraAltIcon from "@mui/icons-material/CameraAlt";
import MenuIcon from "@mui/icons-material/Menu";
import "@fontsource/raleway";

export const Navbar = () => {
  return (
    <Box fontFamily="ABeeZee" sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar style={{ display: "flex", justifyContent: "space-between" }}>
          <Box display={'flex'} alignItems={'center'} gap={2}>
            <IconButton
              size="large"
              edge="start"
              color="inherit"
              aria-label="camera"
            >
              <CameraAltIcon />
            </IconButton>
            <Typography
              color="red"
              variant="h6"
              component="div"
              sx={{ flexGrow: 1 }}
              display={{ xs: "none", lg: "block" }}
            >
              Blog App
            </Typography>
            <Box display={'flex'} gap={2}>
              <Typography
                variant="h6"
                component="div"
                sx={{ flexGrow: 1 }}
                display={{ xs: "none", lg: "block" }}
              >
                <a style={{ color: "blue", textDecoration: 'none' }} href="#">
                  About
                </a>
              </Typography>
              <Typography
                variant="h6"
                component="div"
                sx={{ flexGrow: 1 }}
                display={{ xs: "none", lg: "block" }}
              >
                <a style={{ color: "blue", textDecoration: 'none' }} href="#">
                  Memory
                </a>
              </Typography>
            </Box>
          </Box>
          <Box display={{ xs: "none", lg: "block" }}>
            <Button color="inherit" style={{fontFamily: 'Raleway'}}>Login</Button>
          </Box>
          <Box display={{ xs: "block", lg: "none" }}>
            <IconButton
              size="large"
              edge="start"
              color="inherit"
              aria-label="menu"
            >
              <MenuIcon />
            </IconButton>
          </Box>
        </Toolbar>
      </AppBar>
    </Box>
  );
};
