import { Grid, Box } from '@mui/material';
import { GridContent } from '../GridContent';
import styles from './styles.module.css'
import Side from '../Side';

export const Content = () => {
  return (
    <Box component="section" className={styles.section}>
      <Grid container columnSpacing={1}>
        <Grid item xs={12} md={9}>
          <GridContent/>
        </Grid>
        <Grid display={{xs: 'none', lg: 'block'}} item xs={3}>
          <Side/>
        </Grid>
      </Grid>
    </Box>
  );
}